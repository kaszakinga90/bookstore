package com.kinga.bookstore.book;

import org.springframework.stereotype.Service;

@Service
public record BookService(BookRepository bookRepository) {


    public void createBook(BookDto bookDto) {
        bookRepository.save(toEntity(bookDto));
    }

    private Book toEntity(BookDto bookDto) {

        Book book = new Book();
        book.setAuthor(bookDto.author());
        book.setTitle(bookDto.title());
        return book;
    }

    public BookDto getBook(Long id) throws BookNotFoundExcecption {
        return new BookDto(bookRepository.findById(id).orElseThrow(BookNotFoundExcecption::new));
    }
}
