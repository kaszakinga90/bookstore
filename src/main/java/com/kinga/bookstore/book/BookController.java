package com.kinga.bookstore.book;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/book")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping
    public void createBook(@RequestBody BookDto bookDto){
        bookService.createBook(bookDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookDto> getBook(@PathVariable Long id) {
        try {
            return new ResponseEntity<BookDto>(bookService.getBook(id), HttpStatus.OK);
        } catch (BookNotFoundExcecption e) {
            return new ResponseEntity<BookDto>(HttpStatus.NOT_FOUND);
        }

    }


}
