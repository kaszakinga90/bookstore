package com.kinga.bookstore.book;

public record BookDto(String author, String title) {

    public BookDto(Book book){
        this(book.getAuthor(), book.getTitle());
    }
}
